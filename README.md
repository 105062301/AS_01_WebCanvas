# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

    Report:
        功能(在網頁中點選相應圖示的按鈕即可使用，筆刷、橡皮擦...等功能。)
            1.筆刷:根據當時頁面的顏色和筆刷大小來畫出線條。

            2.橡皮擦:根據筆刷大小來清除畫布上的顏色(並非塗成白色)。

            3.筆刷大小控制:利用brush size旁的滑扭來控制筆刷大小(3~60px)

            4.顏色控制:在筆刷大小的滑鈕旁點選，可控制顏色屬性。

            5.文字輸入:點選Print Text按鈕後即會切換至文字輸入工具，在Print Text旁的文字輸入欄填上文字後，將滑鼠移到畫布上點選左鍵，便能夠根據當前屬性在滑鼠座標放上對應樣式的字體。

            6.字型控制:在Font右邊的文字輸入區可輸入希望的字體，或者是在下方Font(Select)點選不同字體進行切換。(預設字型為Arial)

            7.字體大小控制:在Text Size右邊輸入數字來控制大小。

            8.Cursor icon:在畫布以及按鈕上，會根據功能顯示出不同鼠標。

            9.Reset:點下印有開關圖案的按鈕後，即會將畫布回歸最初始狀態。

            10.Different brush shapes:點選對應按鈕，便可根據當前屬性(ex.顏色)畫出矩形、圓形、三角形。

            11.Un/Re-do button:點選按鈕進行上一步、下一步。(當到達上一步或下一步的極限時網頁會跳出alert通知)

            12.Up/Down-load:在網頁左方可將圖案上傳至畫布，或者將當前畫布的圖案下載下來(檔名:image)

            13.Other useful widgets:Transparent旁的滑鈕可以控制形狀的透明度屬性、藉由拉動滑鈕可畫出透明度不同的矩形...等等。在筆刷上雖有效果但是不明顯。(透明度預設為1，不透明)








