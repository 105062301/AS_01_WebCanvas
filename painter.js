var canvas = document.getElementById('Canvas');
        var context = canvas.getContext('2d');
        var flag = 0, downx, downy;
        var penflag = 0, eraserflag = 0, rectangleflag = 0, circleflag = 0, triangleflag = 0, textflag = 0;
        var textwidth;
        var url = new Array(2000);
        var color;
        var stepnow = 0, stepmax = 0;
        var text, typeface;
        var brushsize, transparent;
             
        function writeMessage(canvas, message) {
        var context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.font = '18pt Calibri';
        context.fillStyle = 'black';
        context.fillText(message, 10, 25);
      }
      function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: evt.clientX - rect.left,
          y: evt.clientY - rect.top
        };
      }

      canvas.addEventListener('mousemove', function(evt){
        var mousePos = getMousePos(canvas, evt);
        if(flag){ 
            if(penflag){                           
                context.lineTo(mousePos.x, mousePos.y);
                context.lineWidth = brushsize;
                context.strokeStyle = color;
                context.globalAlpha = transparent;
                //$('canvas').css({'opacity': transparent});
                context.stroke();  
                context.globalAlpha = 1;                                                                  
            } 
            else if(eraserflag){
                context.clearRect(mousePos.x, mousePos.y, brushsize, brushsize);
            } 
            else if(rectangleflag){
                context.clearRect(0,0,600,600);
                loadImage(stepnow);
                context.beginPath();
                context.lineWidth = brushsize;
                context.strokeStyle = color;
                context.fillStyle = color;
                context.globalAlpha = transparent;
                context.fillRect(downx, downy, mousePos.x-downx, mousePos.y-downy);
                context.stroke();
                context.globalAlpha = 1;
            } 
            else if(circleflag){
                context.clearRect(0,0,600,600);
                loadImage(stepnow);
                context.beginPath();
                var r = mousePos.x-downx;
                if(r<0){
                    r = -1*r;
                }
                context.arc(downx, downy, r, 0, 2*Math.PI);
                //context.fillStyle = 'yellow';
                //context.fill();
                context.lineWidth = brushsize;
                context.strokeStyle = color;
                context.fillStyle = color;
                context.globalAlpha = transparent;
                context.fill();
                context.stroke();
                context.globalAlpha = 1;
            }  
            else if(triangleflag){
                context.clearRect(0,0,600,600);
                loadImage(stepnow);
                context.beginPath();
                context.moveTo(downx, downy);
                context.lineTo(downx, mousePos.y);
                context.lineTo(mousePos.x, mousePos.y);
                context.closePath();
                context.lineWidth = brushsize;
                context.strokeStyle = color;
                context.fillStyle = color;
                context.globalAlpha = transparent;
                context.fill();
                context.stroke();
                context.globalAlpha = 1;
            }   
            /*else if(resetflag){
                context.clearRect(0, 0, 600, 600);
            } */             
        }
        //var message = text;
        //writeMessage(canvas, message);       
      })
      
      canvas.addEventListener('mousedown', function(evt){   
        var mousePos = getMousePos(canvas, evt);  
        color = document.getElementById("color").value;
        transparent= document.getElementById("transparent").value;
        brushsize = document.getElementById("brushsize").value;
        text = document.getElementById("text").value;
        if(textflag){
            textwidth = document.getElementById("number").value;
            typeface = document.getElementById("typeface").value;
            context.font=textwidth*2+"px "+typeface;
            context.fillStyle = color;
            context.fillText(text,mousePos.x,mousePos.y);
        }
        flag = 1; 
        downx = mousePos.x;
        downy = mousePos.y;
        context.beginPath();
        context.moveTo(mousePos.x, mousePos.y);
      })
 
      canvas.addEventListener('mouseup', function(evt){
        var mousePos = getMousePos(canvas, evt);
        flag = 0;
        if(stepnow==stepmax){
            stepnow++;
            stepmax++;
        }
        else if(stepnow<stepmax){
            stepnow++;
        }
        url[stepnow] = canvas.toDataURL();
        loadImage(stepnow);
        
      })

      function loadImage(stepnow){
          var Image1 = new Image();
          Image1.src = url[stepnow];
          context.drawImage(Image1, 0, 0, 600, 600);
      }
      
    var pen = document.querySelector('#pen');
    pen.addEventListener('click',penopen);   
    function penopen(){
        penflag = 1;
        eraserflag = 0;
        rectangleflag = 0;
        circleflag = 0;
        triangleflag = 0;
        textflag = 0;
        $('canvas').css({'cursor': 'url(Pen.cur),auto'});   
        
    }

    var eraser = document.querySelector('#eraser');
    eraser.addEventListener('click',eraseropen);
    function eraseropen(){
        eraserflag = 1;
        penflag = 0;
        rectangleflag = 0;
        circleflag = 0;
        triangleflag = 0;
        textflag = 0;
        $('canvas').css({'cursor': 'url(Eraser.cur),auto'});
    }

    var rectangle = document.querySelector('#rectangle');
    rectangle.addEventListener('click',rectangleopen);
    function rectangleopen(){
        rectangleflag = 1;
        eraserflag = 0;
        penflag = 0;
        circleflag = 0;
        triangleflag = 0;
        textflag = 0;
        $('canvas').css({'cursor': 'url(3Drectangle.cur),auto'});
    }

    var circle = document.querySelector('#circle');
    circle.addEventListener('click',circleopen);
    function circleopen(){
        circleflag = 1;
        rectangleflag = 0;
        eraserflag = 0;
        penflag = 0;
        triangleflag = 0;
        textflag = 0;
        $('canvas').css({'cursor': 'url(Circle.cur),auto'});
    }

    var triangle = document.querySelector('#triangle');
    triangle.addEventListener('click',triangleopen);
    function triangleopen(){
        triangleflag = 1;
        circleflag = 0;
        rectangleflag = 0;
        eraserflag = 0;
        penflag = 0;
        textflag = 0;
        $('canvas').css({'cursor': 'url(3Dtriangle.cur),auto'});
    }

    var reset = document.querySelector('#reset');
    reset.addEventListener('click',resetopen);
    function resetopen(){
        context.clearRect(0, 0, 600, 600);
        url[stepnow] = canvas.toDataURL();
    }

    var undo = document.querySelector('#undo');
    undo.addEventListener('click',undoopen);
    function undoopen(){
        context.clearRect(0, 0, 600, 600); 
        if(stepnow>0){
            stepnow--;
            loadImage(stepnow);
        }
        else{
            stepnow = 1;
            alert("undo limit");
        }
                   
    }

    var redo = document.querySelector('#redo');
    redo.addEventListener('click',redoopen);
    function redoopen(){
        if(stepnow<stepmax){
            context.clearRect(0, 0, 600, 600);
            stepnow++;
            loadImage(stepnow);
        }
        else{
            alert("redo limit");
        }      
    }

    var type1 = document.querySelector('#type1');
    type1.addEventListener('click',type1open);
    function type1open(){
        document.getElementById("typeface").value = "Times New Roman";
    }

    var type2 = document.querySelector('#type2');
    type2.addEventListener('click',type2open);
    function type2open(){
        document.getElementById("typeface").value = "Courier New";
    }

    var print = document.querySelector('#print');
    print.addEventListener('click',printopen);
    function printopen(){
        textflag = 1;
        triangleflag = 0;
        circleflag = 0;
        rectangleflag = 0;
        eraserflag = 0;
        penflag = 0;
        $('canvas').css('cursor', 'url(text.cur),auto');
    }

    var upload = document.querySelector('#upload');
    upload.addEventListener('change',loadopen);
    function loadopen(){
        var file = upload.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.addEventListener('load', function(e){
            var Image1 = new Image();
            Image1.src = this.result;
            Image1.onload = function(){
                context.drawImage(Image1, 0, 0, 600, 600);
            }
            url[stepnow] = canvas.toDataURL();
        })
    }

    var download = document.querySelector('#download');
    download.addEventListener('click',downloadopen);
    function downloadopen(){
        download.href = canvas.toDataURL();
    }